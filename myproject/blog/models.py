from django.db import models
from django.utils import timezone

class Blog(models.Model):
    title = models.CharField(max_length=55)
    author_name = models.CharField(max_length=25)
    content = models.CharField(max_length=2750)
    pub_date = models.DateTimeField('date published')
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return self.title


class Comment(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    nickname = models.CharField(max_length=25)
    email = models.CharField(max_length=30)
    content = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return "Comment no " + str(self.id)

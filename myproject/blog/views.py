from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.urls import reverse
from django.views import generic
from .models import Blog, Comment
from time import strftime
from random import randint
from django.http import HttpResponseRedirect
from .lipsum import LOREMIPSUM
from django.utils import timezone

def comment(request, blog_id):
    blog = get_object_or_404(Blog, pk=blog_id)
    blog.comment_set.create(nickname=request.POST['nickname'],
                            email=request.POST['email'],
                            content=request.POST['content'],
                            pub_date=timezone.now())

    return HttpResponseRedirect(reverse('blog:detail', args=(blog_id,)))
def boot(request):
    #do all the autofill stuff here
    Blog.objects.all().delete()

    for b in range(6):
        sb = str(b)
        blog = Blog(title = "Blog post #" + sb,
        author_name = "A Person",
        content = LOREMIPSUM[randint(0,200):randint(200,len(LOREMIPSUM))],
        pub_date = timezone.now())
 
        blog.save()
        
        for c in range(b + 1):
            sc = str(c)
            blog.comment_set.create(nickname = "commentguy",

                      email = "commentguy@fakemail.com",
                      content = LOREMIPSUM[0:randint(100,200)],
                      pub_date = timezone.now())  

    return HttpResponseRedirect(reverse('blog:index'))

def index(request):
    recent_blogs = Blog.objects.order_by('-pub_date')[:3]

    d = {'time':strftime('%c'),
        'recent_blogs':recent_blogs}
    return render(request, 'blog/templates/index.html', d)

def detail(request, pk):
    blog = Blog.objects.get(id=pk)
    comments = Comment.objects.order_by('-pub_date').filter(blog_id=pk)
    d = {'comments': comments, 
         'blog':blog,
        'time':strftime('%c'),
    }
    return render(request, 'blog/templates/detail.html', d)

def aboutMe(request):
    return render(request, 'blog/templates/aboutme.html', {'time':strftime('%c'),})

def techtips(request):
    return render(request, 'blog/templates/techtips.html', {'time':strftime('%c'),})

def archive(request):
    blogs = Blog.objects.order_by('-pub_date')
    d = {'time':strftime('%c'),
        'blogs':blogs}
    return render(request, 'blog/templates/archive.html', d)



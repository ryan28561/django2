from django.urls import path

from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('aboutme/', views.aboutMe, name='aboutme'),
    path('techtips/', views.techtips, name='techtips'),
    path('<int:pk>/detail/', views.detail, name='detail'),
    path('archive/', views.archive, name='archive'),
    path('init/', views.boot, name='init'),
    path('comment/<int:blog_id>', views.comment, name='comment'),

]
